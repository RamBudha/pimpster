// Swagger TryItOut Button
const DisableTryItOutPlugin = function () {
    return {
      statePlugins: {
        spec: {
          wrapSelectors: {
            allowTryItOutFor: () => () => false,
          },
        },
      },
    };
  };
  
  const options = {
    swaggerOptions: {
      plugins: [
        DisableTryItOutPlugin,
      ],
    },
  };
  
  module.exports = options;