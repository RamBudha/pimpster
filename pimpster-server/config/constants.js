module.exports = {
    LOGIN_SUCCESS: "",
    SIGNUP_SUCCCESS: "User Registered Successfully",
    VERIFY_EMAIL: "Please Verify Your Email",
    ACTIVATION_LINK_SENT: "Activation Link Has Been Sent To The Email. Please Verify.",
    DUPLICATE_EMAIL_ERROR: "Email Already Registered",
    DUPLICATE_NUMBER_ERROR: "Phone Number Already Registered",

    SCHEMAS: {
      userTypes: {
        CUSTOMER: "CUSTOMER",
        PROVIDER: "PROVIDER",
        ADMIN: "ADMIN",
        SUPERADMIN: "SUPERADMIN"
      },
      userStandard: {
        BRONZE: "BRONZE",
        SILVER: "SILVER",
        GOLD: "GOLD",
        PLATINUM: "PLATINUM",
      },
      userGender: {
        MALE: "MALE",
        FEMALE: "FEMALE",
        LESBIAN: "LESBIAN",
        GAY: "GAY",
        THIRD: "THIRD",
      }
    }
  };