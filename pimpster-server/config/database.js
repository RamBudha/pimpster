const mongoose = require("mongoose");
const { MONGO_URI } = require("./config");

mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);
mongoose.set("useNewUrlParser", true);
mongoose.connect(MONGO_URI);


module.exports = mongoose.connection;