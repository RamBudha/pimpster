
const path = require("path");

require("dotenv").config({ path: path.join(__dirname, "../.env") });

module.exports = {
  ENV: process.env.NODE_ENV,
  PORT: process.env.PORT,
  BASE_URL: (process.env.NODE_ENV === "production" ? process.env.BASE_URL_PROD : process.env.NODE_ENV === "staging" ? process.env.BASE_URL_STAGE : process.env.BASE_URL_DEV),
  MONGO_URI: process.env.NODE_ENV === "test"
    ? process.env.MONGO_URI_TEST : (process.env.NODE_ENV === "development"
      ? process.env.MONGO_URI_DEV : process.env.MONGO_URI_PROD),
  LOGS: process.env.NODE_ENV === "production" ? "combined" : "dev",
  SESSION_SECRET: process.env.SESSION_SECRET,
};