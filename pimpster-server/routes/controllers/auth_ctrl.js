const express = require("express");
const SchemaValidator = require("../../helper/schemaValidator");
const passport = require("../../helper/passport");
const User = require("../../models/User");
const Customer = require("../../models/Customer");
// const Restaurant = require("../../../models/Restaurant");
const { SIGNUP_SUCCCESS,
  DUPLICATE_EMAIL_ERROR,
  DUPLICATE_NUMBER_ERROR
} = require("../../config/constants");

const validateRequest = SchemaValidator(true);

const router = express.Router();
router.post("/login", validateRequest, (req, res, next) => {
  passport.authenticate("localLoginStrategy", (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(404).json({ statusCode: "404", message: info.message });
    }
    req.login(user, (err) => {
      if (err) {
        console.error(err);
        return next(err);
      }
      return res.status(200).json({ statusCode: "200", message: "Login successfull" });
    });
  })(req, res, next);
});

router.post("/signup", validateRequest, async (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const phone = req.body.phone;
  // const address = req.body.address
  const firstname = req.body.firstname;
  const lastname = req.body.lastname;
  const middlename = req.body.middlename || "";
  const signupType = req.body.signupType || "customer";

  //  All fileds are valids when control reach here
  try {
    const results = await Promise.all([User.checkDuplicateEmail(email), User.checkDuplicateNumber(phone)]);

    if (results[0]) return res.status(422).json({ statusCode: "422", message: DUPLICATE_EMAIL_ERROR });
    else if (results[1]) return res.status(422).json({ statusCode: "422", message: DUPLICATE_NUMBER_ERROR });

    const newUser = new User({
      email,
      phone,
      type: signupType,
    });

    // let document = {};

    if (signupType === "customer") {
      //  Customer signup here
      const customer = new Customer({
        userId: newUser._id,
        name: {
          first: firstname,
          last: lastname,
          middle: middlename,
        },
      });
      const savedDocument = await customer.save();
      newUser.roles.customer = savedDocument._id;
    }

    const salt = await newUser.generateSalt();
    const hash = await newUser.encryptPassword(password, salt);

    newUser.salt = salt;
    newUser.hash = hash;

    await newUser.save();
    return res.status(200).json({ statusCode: 200, message: SIGNUP_SUCCCESS });
  } catch (err) {
    next(err);
  }
});

module.exports = router;