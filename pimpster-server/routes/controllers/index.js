const router = require("express").Router();

const authRouter = require("./auth_ctrl");

router.use("/auth", authRouter);


router.use((err, _req, res, next) => {
  if (err.name === "ValidationError") {
    const mongooseError = {
      statusCode: "422",
      message: "Validation Error",
      errors: Object.keys(err.errors).reduce((obj, key) => {
        obj[key] = err.errors[key].message.replace(/[``]/g, "");
        return obj;
      }, {}),
    };

    return res.status(422).json(mongooseError);
  }
  return next(err);
});

module.exports = router;