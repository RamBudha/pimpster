const router = require("express").Router();
const swaggerUi = require("swagger-ui-express");

// Swagger Middleware
const swaggerDocument = require("../swagger/swagger.json");
const options = require("../swagger/swagger");

router.use("/api", require("./controllers"));

router.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument, options), router);
router.get("/", (req, res) => res.send("<html><title>Pimpster</title><body><h3>Welcome to Pimpster API</h3><p>Check out the <a href='/docs'>docs</a> to get started.</p></body></html>"));


module.exports = router;