const _ = require("lodash");
const Joi = require("joi");
const Schemas = require("./schemas");

module.exports = () => {
  const _supportedMethods = ["post", "put"];

  const _validationOptions = {
    abortEarly: false,
    allowUnknown: true, // if false, it means donot allow keys other than defined in schema,
    //  will throw error if there are any.
    stripUnknown: true, // if true, it means strip all the keys tht are not part of the schemas
  };

  return (req, res, next) => {
    const route = req.route.path;
    const method = req.method.toLowerCase();

    if (_.includes(_supportedMethods, method) && _.has(Schemas, route)) {
      const _schema = _.get(Schemas, route);
      if (_schema) {
        return Joi.validate(req.body, _schema, _validationOptions, (err, data) => {
          if (err) {
            const JoiError = {
              statusCode: "422",
              message: "Validation Error",
              errors: err.details.reduce((obj, { path, message }) => {
                obj[path[0]] = message.replace(/[""]/g, "");
                return obj;
              }, {}),
            };

            res.status(422).json(JoiError);
          } else {
            //  since we are setting strip unknown to true
            //  the "data" contains the stripped out values
            req.body = data;
            next();
          }
        });
      }
    }
    next();
  };
};