const Joi = require("joi");

const name = Joi.string().regex(/^[a-zA-Z]+$/, "alphabets").lowercase();
const email = Joi.string().email().label("Email").lowercase()
  .required();
const phone = Joi.string().length(10).regex(/^[0-9]+$/, "numbers");
const password = Joi.string().label("Password").required();
// const ageSchema = Joi.alternatives().try([
//   Joi.number().integer().greater(6).required(),
//   Joi.string().replace(/^([7-9]|[1-9]\d+)(y|yr|yrs)?$/i, "$1").required(),
// ]);
const showCustomErrorMessage = function (errors) {
  errors.forEach((err) => {
    switch (err.type) {
      case "any.empty":
        err.message = `${err.context.label} is required`;
        break;
      case "string.email":
        err.message = `${err.context.label} is invalid`;
        break;
      case "string.regex.name":
        err.message = `${err.context.label} must contain ${err.context.name} only`;
        break;
      case "string.length":
        err.message = `${err.context.label} must be of length ${err.context.limit}`;
        break;
      case "number.integer":
        err.message = `${err.context.label} must be number`;
        break;
      case "string.allowOnly":
        err.message = `${err.context.label} should contain any of ${err.context.valids}`;
        break;
      default:
        break;
    }
  });
  return errors;
};

// const personDataSchema = Joi.object().keys({
// //   id: personID.required(),
//   firstname: name.error(() => "Invalid First Name"),
//   lastname: name.error(() => "Invalid Last name"),
//   fullname: Joi.string().regex(/^[A-Z]+ [A-Z]+$/i).uppercase(),
//   type: Joi.string().valid("STUDENT", "TEACHER").uppercase().required(),
//   sex: Joi.string().valid(["M", "F", "MALE", "FEMALE"]).uppercase().required(),

//   age: Joi.when("type", {
//     is: "STUDENT",
//     then: ageSchema.required(),
//     otherwise: ageSchema,
//   }),
// }).xor("firstname", "fullname")
//   .and("firstname", "lastname")
//   .without("fullname", ["firstname", "lastname"]);


const loginDataSchema = Joi.object({
  email: Joi.string().email().label("Email").lowercase()
    .required()
    .error(showCustomErrorMessage),
  password: Joi.string().label("Password")
    .required()
    .error(showCustomErrorMessage),
});

const signupDataSchema = Joi.object({
  email: email.error(showCustomErrorMessage),
  phone: phone.label("Phone").required().error(showCustomErrorMessage),
  password: password.error(showCustomErrorMessage),
  firstname: name.label("Firstname").required().error(showCustomErrorMessage),
  middlename: name.optional().error(showCustomErrorMessage),
  lastname: name.label("Lastname").required().error(showCustomErrorMessage),
  signupType: name.label("Signup Type").optional().valid(["customer", "restaurant"]).error(showCustomErrorMessage),
});

// const feesDataSchema = Joi.object({
// //   studentId: personID.required(),
//   amount: Joi.string().creditCard().required(),
//   cardNumber: Joi.string().creditCard().required(),
//   completedAt: Joi.date().timestamp().required(),
// });


module.exports = {
  "/login": loginDataSchema,
  "/signup": signupDataSchema,
};