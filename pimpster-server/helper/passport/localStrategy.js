const LocalStrategy = require("passport-local").Strategy;
const User = require("../../models/User");

const strategy = new LocalStrategy(
  {
    usernameField: "email",
    passwordField: "password",
    passReqToCallback: true,

  },
  ((req, email, password, done) => {
    User.findOne({ email }, async (err, user) => {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false, { message: "Incorrect Email or Password" });
      }
      const isValid = await user.comparePassword(password);
      if (!isValid) {
        return done(null, false, { message: "Incorrect  Email or Password" });
      }
      // check for if the user is active
      // check if the email is activated
      // check if user has excessed login limit
      // if all okay return success
      return done(null, user);
    });
  }),
);

module.exports = strategy;