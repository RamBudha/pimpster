const passport = require("passport");
const LocalStrategy = require("./localStrategy");
const User = require("../../models/User");

// called on login, saves the id to session req.session.passport.user = {id:".."}
passport.serializeUser((user, done) => {
  console.log("*** serializeUser called, user: ");
  console.log(user); // the whole raw user object!
  console.log("---------");
  done(null, user._id);
});

// user object attaches to the request as req.user
passport.deserializeUser((_id, done) => {
  console.log("DeserializeUser called");
  User.findById(_id, (err, user) => {
    if (err) {
      console.error("There was an error accessing the records of user with id:", _id);
      console.error(err.message);
      // return console.log(err.message);
    }
    console.log("*** Deserialize user, user:");
    console.log(user);
    console.log("--------------");
    done(null, user);
  });
});

//  Use Strategies
passport.use("localLoginStrategy", LocalStrategy);

module.exports = passport;