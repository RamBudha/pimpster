const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const SCHEMAS = require("../config/constants").SCHEMAS;

// const ObjectId = mongoose.Schema.Types.ObjectId;

const userSchema = new mongoose.Schema({
  name: {
    first: { type: String, default: "" },
    middle: { type: String, default: "" },
    last: { type: String, default: "" },
  },
  phone: {
    type: String,
    match: /^[0-9]+$/,
    required: true,
    unique: true,
    trim: true,
    maxlength: 10,
  },
  email: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    required: true,
    unique: true,
    trim: true,
    lowercase: true,
  },
  profileImage: {
    type: String,
    default: null,
  },
	gender: {
    type: String,
    default: SCHEMAS.userGender.MALE,
		enum: [
      SCHEMAS.userGender.MALE,
      SCHEMAS.userGender.FEMALE,
      SCHEMAS.userGender.LESBIAN,
      SCHEMAS.userGender.GAY,
      SCHEMAS.userGender.THIRD,
    ],
	},
  hash: {
    type: String,
    required: true,
    // maxlength: 128,
  },
  salt: {
    type: String,
    required: true,
    // maxlength: 128,
  },
  // roles: {
  //   // admin: { type: ObjectId, ref: "Admin" },
  //   customer: {
  //     type: ObjectId,
  //     ref: "Customer"
  //   },
  //   // restaurant: { type: ObjectId, ref: "Restaurant" },
  // },
  userType: {
    type: String,
    required: true,
    default: SCHEMAS.userTypes.CUSTOMER,
    enum: [
      SCHEMAS.userTypes.CUSTOMER,
      SCHEMAS.userTypes.PROVIDER,
      SCHEMAS.userTypes.ADMIN
    ],
  },
  userRating: {
    type: Number,
    default: 2
  },
  ratedBy: {
    type: Number,
    default: 0
  },
  isActive: {
    type: Boolean,
    default: false,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
  joinedSince: {
    type: Date,
  },
  location: {
		type: { type: String, default: "Point" },
		coordinates: { type: Array, default: [0, 0] }
  },

  emailActivationToken: String,
  emailActivationExpires: Date,
  resetPasswordToken: String,
  resetPasswordExpires: Date,
}, { timestamps: true });

const generateHash = (password, salt) => bcrypt.hash(password, salt);
userSchema.methods.generateSalt = () => bcrypt.genSalt(10);

userSchema.methods.encryptPassword = (password, salt) => {
  // const salt = await this.generateSalt();
  const hash = generateHash(password.toString(), salt);
  return hash;
};

userSchema.methods.comparePassword = async function (password) {
  console.log(password, this.salt, "in compare password");
  const hash = await generateHash(password, this.salt);
  console.log(hash);
  return hash === this.hash;
};

userSchema.statics.checkDuplicateEmail = function (email) {
  return this.findOne({ email });
};

userSchema.statics.checkDuplicateNumber = function (number) {
  return this.findOne({ phone: number });
};

userSchema.index({ phone: 1, email: 1 });
module.exports = mongoose.model("User", userSchema);