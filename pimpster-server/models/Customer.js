const mongoose = require("mongoose");
const SCHEMAS = require("../config/constants").SCHEMAS.userStandard;
const customerSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: [true, "User Id is required"],
  },
  profile: { type: String, default: "" },
  address: {
    state: { type: String, default: "" },
    city: { type: String, default: "" },
    zipcode: { type: String, default: "" },
    localBody: { type: String, default: "" },
    district: { type: String, default: "" },
    houseNum: { type: String, default: "" },
    address: { type: String, default: "" },
  },
  dateOfBirth: {
    type: Date
  },
  userStandard: {
    type: String,
    enum: [
      SCHEMAS.BRONZE,
      SCHEMAS.SILVER,
      SCHEMAS.GOLD,
      SCHEMAS.PLATINUM
    ]
  },
  isAllDocsVerified: {
		type: Boolean,
		default: false
	},

});
customerSchema.index({ userId: 1 });

module.exports = mongoose.model("Customer", customerSchema);