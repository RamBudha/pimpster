const express = require("express");
const compression = require("compression");
const session = require("express-session");
const bodyParser = require("body-parser");
const helmet = require("helmet");
const logger = require("morgan");
const chalk = require("chalk");
const errorHandler = require("errorhandler");
const cors = require("cors");
const MongoStore = require("connect-mongo")(session);

const passport = require("./helper/passport");
const dbConnection = require("./config/database");
const { ENV, PORT, BASE_URL, LOGS, SESSION_SECRET } = require("./config/config");
const router = require("./routes");

const isProduction = ENV === "development";

const app = express();
app.disable("x-powered-by");

app.set("port", PORT);
app.set("env", ENV);
app.set("base_url", BASE_URL);

dbConnection.on("error", (err) => {
  console.error(err);
  console.log("%s MongoDB connection error. Please make sure MongoDB is running.", chalk.red("✗"));
  process.exit();
});

dbConnection.once("connected", () => {
  console.log("%s Successfully connected to the database", chalk.green("✓"));
});

dbConnection.once("disconnected", () => {
  console.log("%s Successfully disconnected from database", chalk.red("✗"));
});

app.use(compression());

app.use(helmet());

app.use(cors());

app.use(logger(LOGS));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(session({
  name: "pimpster.session",
  store: new MongoStore({ mongooseConnection: dbConnection }),
  secret: SESSION_SECRET,
  touchAfter: 24 * 3600,
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 1000 * 60 * 15 },
}));

app.use(passport.initialize());
app.use(passport.session());


app.use("/", router);

if (!isProduction) {
  app.use(errorHandler());
}

app.use((req, res, next) => {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
});


if (!isProduction) {
  app.use((err, req, res) => {
    console.log(err.stack);
    const status = err.status || 500;
    return res.status(status).json({ statusCode: status, message: err.message });
  });
}


app.use((err, req, res) => {
  const status = err.status || 500;
  return res.status(status).json({ statusCode: status, message: err.message });
});


app.listen(PORT, () => {
  console.log("%s App is running at %s:%d in %s mode", chalk.green("✓"), BASE_URL, PORT, ENV);
});

module.exports = app;
